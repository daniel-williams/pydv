.. pydv documentation master file, created by
   sphinx-quickstart on Fri Apr  7 14:14:15 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to pydv's documentation!
================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   installation
   tutorial
   channellist
   configuration
   
`pydv` is a new python-based library for producing plots of
Gravitational Wave data from interferometers.

`pydv` collects data using the `LIGO NDS protocol
<https://www.lsc-group.phys.uwm.edu/daswg/projects/nds-client.html>`_,
and is able to plot any channel which is stored on an NDS server.

Features
========

* Quick command-line plotting of NDS channels
* Plotting pre-defined channel lists
* Plotting user-defined channel lists.


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
