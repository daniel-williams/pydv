.. Documentation of the configuration settings for pydv

====
pydv
====

A number of configuration settings can be made using dotfiles and
configuration files for `pydv`.

By default `pydv` will search for a file called ``.pydv``, first in
the present working directory, and then in the user's home directory,
before falling back on the default configuration provided with the
package.

Server addresses
================

The server addresses used to pull NDS data are stored under the
servers section in the configuration file. The server labelled
``None`` is the fallback if the correct server for the channel cannot
be detected. The default configuration is ::

  [servers]
  None: nds.ligo.caltech.edu, 31200
  L1: nds.ligo-la.caltech.edu, 31200
  H1: nds.ligo-wa.caltech.edu, 31200
  V1: nds.ligo.caltech.edu, 31200


A different server can be used by changing the address and port as
appropriate, for example, to use the control room NDS server for L1 we
could change the address by setting this::

    [servers]
    L1: l1nds0, 8088
