.. Instructions for installing the data viewer

=================
Installing `pydv`
=================

In order to install and run `pydv` you'll need to have access to the
``nds2-client`` software produced by LIGO.

The recommended method for installing `pydv` is to use a python
virtual enviroment, which allows pydv and its dependencies to be
isolated from the rest of the system it's installed on, so that it can
be easily updated and uninstalled.

A guide to using virtual environments can be found in the `Hitchhikers
guide to Python
<http://python-guide-pt-br.readthedocs.io/en/latest/dev/virtualenvs/>`_.

You can fetch the code for `pydv` from the LIGO gitlab server using ::

  git clone git@git.ligo.org:daniel-williams/pydv.git

The program can then be installed within the virtual environment using ::

  python setup.py install


