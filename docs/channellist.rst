.. This is the documentation for the pydv channel list format.
   These channel list are based on the channel lists used for
   the ligo summary pages.
========================
Dataviewer Channel Lists
========================

It can be useful to have quick access at the command line to a large
number of different channels. pydv allows you to define lists of
channels which should be plotted, either together on the same plot, or
on different plots, or some combination of the two.

The basic installation of pydv comes supplied with some pre-made
channel-lists, some of which are inspired by the LIGO detchar summary
pages, and some are designed to replicate frequent control room tasks.

You can run one of those built-in lists by running, a command of the form::

  $ pydv list <list name>

`pydv` will search through your configuration file, and its default
configuration for the list, and if it finds it, will perform the nds2
data requests and will plot the data.

Adding new channel lists
========================

You can add new channel lists to your `pydv` installation either by
adding them to your own configuration file (stored as ``.pydv`` in
your home directory), or in a seperate file using the same format.

An example channel list looks something like::

  [list-calibration]
  name: Calibration
  shortname: Calibration
  0= L1:CAL-CS_TDEP_KAPPA_TST_REAL_OUTPUT,
  L1:CAL-CS_TDEP_KAPPA_TST_IMAG_OUTPUT
  0-title=CAL-CS_TDEP_KAPPA_TST
  1= L1:CAL-CS_TDEP_KAPPA_PU_REAL_OUTPUT,
  L1:CAL-CS_TDEP_KAPPA_PU_IMAG_OUTPUT
  1-title=CAL-CS_TDEP_KAPPA_PU
  2= L1:CAL-CS_TDEP_A_REAL_OUTPUT,
  L1:CAL-CS_TDEP_A_IMAG_OUTPUT
  2-title=CAL-CS_TDEP_A
  3= L1:CAL-CS_TDEP_KAPPA_C_OUTPUT
  3-title=CAL-CS_TDEP_KAPPA_C
  4= L1:CAL-CS_TDEP_F_C_OUTPUT
  4-title=CAL-CS_TDEP_F_C

The block starts with the name of the list in square brackets,
starting with ``list-``. When you call the channel list you only need
the string following the hyphen, however, so this list can be plotted
by running::
  $ pydv list calibration

Each subplot is defined by a line which starts with a number, for
example::

  0= L1:CAL-CS_TDEP_KAPPA_TST_REAL_OUTPUT,
  L1:CAL-CS_TDEP_KAPPA_TST_IMAG_OUTPUT

Produces a subplot with two channels. We also need to specify a title
for this subplot by setting the variable ``0-title``. This can be left
blank, but is required.

Storing channel list files
==========================

Channel lists can be stored in this format in a separate file, and loaded by calling::

  pydv list <path-to-file>

Only one channel list should be stored in a file, under the
configuration heading ``[list]``, out previous example would look like
this if stored in a channel-list file.::

    [list]
    name: Calibration
    shortname: Calibration
    0= L1:CAL-CS_TDEP_KAPPA_TST_REAL_OUTPUT,
    L1:CAL-CS_TDEP_KAPPA_TST_IMAG_OUTPUT
    0-title=CAL-CS_TDEP_KAPPA_TST
    1= L1:CAL-CS_TDEP_KAPPA_PU_REAL_OUTPUT,
    L1:CAL-CS_TDEP_KAPPA_PU_IMAG_OUTPUT
    1-title=CAL-CS_TDEP_KAPPA_PU
    2= L1:CAL-CS_TDEP_A_REAL_OUTPUT,
    L1:CAL-CS_TDEP_A_IMAG_OUTPUT
    2-title=CAL-CS_TDEP_A
    3= L1:CAL-CS_TDEP_KAPPA_C_OUTPUT
    3-title=CAL-CS_TDEP_KAPPA_C
    4= L1:CAL-CS_TDEP_F_C_OUTPUT
    4-title=CAL-CS_TDEP_F_C

    
