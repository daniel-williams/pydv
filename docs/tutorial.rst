.. This is the tutorial for simple use of the pydv software

========
Tutorial
========

Say you want to plot the calibrated strain data from the Livingston
detector to have a quick look at the last 10 seconds of data. We can
do this by running ::

  pydv channel L1:DMT-CALIB_STRAIN

provided that we have access to the live data. If we want to have a
look at the data around a specific GPS time we can do that too, by
adding the ``--time <gps-time>`` flag to the ``pydv`` command.::

  pydv --time 1181406148 channel L1:DMT-CALIB_STRAIN

We can adjust the amount of data which is collected an plotted using
the ``--lookback`` and ``--lookforward`` flags to fetch and plot data
on either side of the requested time. For example::

  pydv --time 1181406148 --lookback 20 --lookforward 20 channel L1:DMT-CALIB_STRAIN

Will fetch 20 seconds' worth of data on either side of ``1181406148``.

Plotting several channels
=========================

We can plot multiple channels from the command line by separating
their names with commas, for example ::

  pydv channel L1:CAL-CS_TDEP_KAPPA_TST_REAL_OUTPUT,L1:CAL-CS_TDEP_KAPPA_TST_IMAG_OUTPUT

The same flags can be added as with a single channel in order to fetch
more or less data than the default 10 seconds before the requested
time.
