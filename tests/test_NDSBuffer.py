import sys
import unittest
import logging
import numpy as np

from gpstime import tconvert

from pydv.bufferdict import NDSBuffer, NDSBufferDict

START_TIME = 1175448333
BUFFER_LENGTHS_IN_SECONDS = 10
END_TIME = START_TIME + BUFFER_LENGTHS_IN_SECONDS

CHANNELS = ['L1:SEI-Y_RTD_STAGE0_SENSORA',
            'L1:SEI-Y_RTD_STAGE0_SENSORB',
            'L1:SEI-Y_RTD_STAGE1_SENSORA',
            'L1:SEI-Y_RTD_STAGE1_SENSORB',
            'L1:SEI-Y_RTD_STAGE2_SENSORB',
            'L1:SEI-Y_RTD_STAGE2_SENSORA',
            'L1:GDS-CALIB_STRAIN',
            'H1:PSL-ISS_PDA_OUT16',
            'L1:PSL-ISS_PDB_OUT16'
]

def _add_stdout_logger(self, logger):
    formatter = logging.Formatter('        %(name)s - %(levelname)s - %(message)s')
    handler = logging.StreamHandler(stream=sys.stdout)
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    logger.setLevel(logging.DEBUG)

class TestNDSBufferDict(unittest.TestCase):
    def setUp(self):
        #_add_stdout_logger(NDSBufferDict.logger)
        #_add_stdout_logger(NDSBuffer.logger)

        #self.recent_time = float(tconvert('now'))-SECONDS_AGO_TO_TEST_AROUND

        pass
    
    @staticmethod
    def buffers_are_equal(c, d):
        if d.is_wd_state != c.is_wd_state:
            return False
        if d.channel_name != c.channel_name:
            return False
        if d.sample_rate != c.sample_rate:
            return False
        if d.start_time != c.start_time:
            return False
        if d.end_time != c.end_time:
            return False
        if not np.array_equal(d.data, c.data):
            return False
        if not np.array_equal(d.time_domain, c.time_domain):
            return False
        return True

    @staticmethod
    def buffer_dicts_are_equal(A, B):
        class Failed(Exception): pass
        try:
            if sorted(A.channel_names) != sorted(B.channel_names):
                raise Failed
            for channel_name in A.channel_names:
                if not TestNDSBufferDict.buffers_are_equal(A[channel_name], B[channel_name]):
                    raise Failed
        except Failed:
            return False
        return True

    def test_equality_method(self):
        A = NDSBufferDict(['H1:ISI-ITMX_ST1_CPSINF_H1_IN1_DQ'], START_TIME, END_TIME)
        B = NDSBufferDict(['H1:ISI-ITMX_ST1_CPSINF_H1_IN1_DQ'], START_TIME, END_TIME)
        self.assertTrue(TestNDSBufferDict.buffer_dicts_are_equal(A, B))

    def _get_recent_buffer(self, start, end):
        assert(end <= END_TIME)
        return NDSBufferDict(['H1:ISI-ITMX_ST1_CPSINF_H1_IN1_DQ'],
                             START_TIME, END_TIME).buffers[0]

    def test_buffer_merge_disjoint(self):
        B = dict()
        for args in [(0,BUFFER_LENGTHS_IN_SECONDS-5), (BUFFER_LENGTHS_IN_SECONDS-5,BUFFER_LENGTHS_IN_SECONDS), (0,BUFFER_LENGTHS_IN_SECONDS)]:
            B[args] = self._get_recent_buffer(*args)
        self.assertTrue(TestNDSBufferDict.buffers_are_equal(B[(0,BUFFER_LENGTHS_IN_SECONDS-5)]+B[(BUFFER_LENGTHS_IN_SECONDS-5,BUFFER_LENGTHS_IN_SECONDS)], B[(0,BUFFER_LENGTHS_IN_SECONDS)]))

    def test_buffer_merge_overlapping_left(self):
        B = dict()
        for args in [(0,BUFFER_LENGTHS_IN_SECONDS-3), (BUFFER_LENGTHS_IN_SECONDS-7,BUFFER_LENGTHS_IN_SECONDS), (0,BUFFER_LENGTHS_IN_SECONDS)]:
            B[args] = self._get_recent_buffer(*args)
        self.assertTrue(TestNDSBufferDict.buffers_are_equal(B[(0,BUFFER_LENGTHS_IN_SECONDS-3)]+B[(BUFFER_LENGTHS_IN_SECONDS-7,BUFFER_LENGTHS_IN_SECONDS)], B[(0,BUFFER_LENGTHS_IN_SECONDS)]))

    def test_buffer_merge_overlapping_right(self):
        B = dict()
        for args in [(0,BUFFER_LENGTHS_IN_SECONDS-3), (BUFFER_LENGTHS_IN_SECONDS-7,BUFFER_LENGTHS_IN_SECONDS), (0,BUFFER_LENGTHS_IN_SECONDS)]:
            B[args] = self._get_recent_buffer(*args)
        self.assertTrue(TestNDSBufferDict.buffers_are_equal(B[(BUFFER_LENGTHS_IN_SECONDS-7,BUFFER_LENGTHS_IN_SECONDS)]+B[(0,BUFFER_LENGTHS_IN_SECONDS-3)], B[(0,BUFFER_LENGTHS_IN_SECONDS)]))

    def test_buffer_merge_superset(self):
        B = dict()
        for args in [(BUFFER_LENGTHS_IN_SECONDS-13,BUFFER_LENGTHS_IN_SECONDS-3), (0,BUFFER_LENGTHS_IN_SECONDS)]:
            B[args] = self._get_recent_buffer(*args)
        self.assertTrue(TestNDSBufferDict.buffers_are_equal(B[(BUFFER_LENGTHS_IN_SECONDS-13,BUFFER_LENGTHS_IN_SECONDS-3)]+B[(0,BUFFER_LENGTHS_IN_SECONDS)], B[(0,BUFFER_LENGTHS_IN_SECONDS)]))

    def test_buffer_dict_lots_of_channels_equality(self):
        A = NDSBufferDict(CHANNELS, START_TIME, END_TIME)
        A.add_data(START_TIME+BUFFER_LENGTHS_IN_SECONDS-5, START_TIME+BUFFER_LENGTHS_IN_SECONDS)
        B = NDSBufferDict(CHANNELS, START_TIME, END_TIME)
        self.assertTrue(TestNDSBufferDict.buffer_dicts_are_equal(A, B))

        
if __name__ == '__main__':
    unittest.main()
