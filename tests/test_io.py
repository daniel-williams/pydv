"""

This file is part of the pydv test-suite.

Author: Daniel Williams <daniel.williams@ligo.org>

IO Tests
--------

Tests for the IO operations of the dataviewer.

"""

import unittest
import pydv
import nds2
from pydv import io
from gpstime import tconvert
import numpy as np

START_TIME = 1175448333
END_TIME = START_TIME + 100

class TestIO(unittest.TestCase):
    def setUp(self):
        self.ndsio = io.NDSClient()
    
    def test_connection(self):
        """
        Test that an NDS connection can be established correctly to
        each of the data servers.
        """
        
        self.assertTrue(self.ndsio.connection("L1"))
        self.assertTrue(self.ndsio.connection("H1"))
        
    def test_pull_one_channel(self):
        """
        Pull the data from a single channel.
        """
        self.assertTrue(self.ndsio.fetch("L1:GDS-CALIB_STRAIN",  START_TIME, END_TIME))

    def test_pull_two_channels(self):
        """
        Pull the data from two channels.
        """
        self.assertTrue(self.ndsio.fetch(['L1:PSL-ISS_PDA_OUT16', 'L1:PSL-ISS_PDB_OUT16'], START_TIME, END_TIME))

    def test_pull_two_sites(self):
        """
        Pull data from two different sites in the same query.
        """
        self.assertTrue(self.ndsio.fetch(['H1:PSL-ISS_PDA_OUT16', 'L1:PSL-ISS_PDB_OUT16'], START_TIME, END_TIME))

    def test_pull_more_than_six(self):
        """
        Apparently NDS doesn't play well when more than six channels are included in a query.
        """
        self.assertTrue(self.ndsio.fetch(['L1:SEI-Y_RTD_STAGE0_SENSORA','L1:SEI-Y_RTD_STAGE0_SENSORB','L1:SEI-Y_RTD_STAGE1_SENSORA','L1:SEI-Y_RTD_STAGE1_SENSORB','L1:SEI-Y_RTD_STAGE2_SENSORB','L1:SEI-Y_RTD_STAGE2_SENSORA','L1:GDS-CALIB_STRAIN'], START_TIME, END_TIME))

class TestRingBuffer(unittest.TestCase):
    """
    Tests for the ring buffer
    """
    def setUp(self):
        # Make a length 10 ring buffer
        self.buffer = io.RingBuffer(10)

    def test_extension(self):
        data = np.array([10, 3, 4])
        self.buffer.extend(data)
        self.assertEqual(self.buffer.get()[-1], 4)

    def test_length_update(self):
        data = np.array([10, 3, 4])
        self.buffer.extend(data)
        self.buffer.setLength(8)
        data = self.buffer.data
        self.assertEqual(len(data), 8)
        self.assertEqual(self.buffer.get()[-1], 4)

    
        
if __name__ == '__main__':
    unittest.main()
