#!/usr/bin/env python

# much of the structure here was cribbed from
# https://github.com/pypa/sampleproject

from setuptools import setup, find_packages
# To use a consistent encoding
from codecs import open
from os import path

here = path.abspath(path.dirname(__file__))

# Get the long description from the README file
with open(path.join(here, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()

version = {}
with open("pydv/version.py") as f:
    exec(f.read(), version)

setup(
    name = 'pydv',
    version = version['__version__'],
    description = 'NDS timeseries plotting utility.',
    long_description = long_description,
    author = 'Charles Celerier',
    author_email = 'charles.celerier@ligo.org',
    url = 'https://git.ligo.org/CDS/pydv',
    license = 'GPLv3+',
    include_package_data=True,
    packages = ['pydv'],
    zip_safe=True,
    keywords = ['nds', 'nds2', 'plotting'],
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: End Users/Desktop',
        'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)',
        'Programming Language :: Python :: 2 :: Only',
        'Operating System :: POSIX',
        # FIXME: also works to produce plots from command line
        'Environment :: X11 Applications',
        ],
    #dependency_links = ['https://git.ligo.org/jameson.rollins/gpstime/repository/archive.tar.gz?ref=0.2'],
    #install_requires = [
    #     'matplotlib',
    #    'numpy',
    #    'scipy',
    #     ],
    # You can install this optional dependency using the following syntax:
    # $ pip install -e .xdo
    #extras_require={
    #     'docs': ['Sphinx', 'numpydoc'],
    # },
    # https://chriswarrick.com/blog/2014/09/15/python-apps-the-right-way-entry_points-and-scripts/
    # should we have a 'gui_scripts' as well?
    entry_points={
        'console_scripts': [
            'pydv = pydv.__main__:main',
        ],
    },
    #test_suite='tests',

)
