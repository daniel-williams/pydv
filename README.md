pydv: python NDS data plotting tool
===================================

Python library for creating time series plots of LIGO data retrieved
using the LIGO NDS services.


The following packages are required to run pydv:

  * [matplotlib](http://matplotlib.org/)
  * [nds2-client](https://trac.ligo.caltech.edu/nds2/)
  * [gpstime](https://git.ligo.org/jameson.rollins/gpstime)
