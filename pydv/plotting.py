import re
import numpy as np
from matplotlib.ticker import Formatter, MaxNLocator
from matplotlib import cbook
from matplotlib.patches import Rectangle
import matplotlib.transforms as mtransforms
from matplotlib.figure import Figure
from matplotlib.axes import Axes

from gpstime import tconvert

from . import util
from . import const
from .bufferdict import NDSBufferDict

from pkg_resources import resource_string, resource_stream, resource_filename
default_style = resource_filename(__name__, 'pydv-style.mpl')
import matplotlib.pyplot as plt
plt.style.use(default_style)

def _get_wd_state_key(fig):
    labels = []
    handles = []
    for state in const.WD_STATES:
        handles.append(Rectangle((0, 0), 1, 1, facecolor=const.WD_STATE_COLOR[state],
                       alpha=1.5*const.WD_STATE_SHADING_SETTINGS['alpha']))
        labels.append(str(state))

    legend = fig.legend(handles, labels, title='Watchdog state key')
    return legend


def _remove_wd_state_shading(ndsaxes):
    for shaded_region in ndsaxes.shaded_wd_state_regions:
        shaded_region.remove()


def _update_wd_state_shading(ndsaxes, channel_name, time_domain, data, end_time):
    _remove_wd_state_shading(ndsaxes)

    #shade_settings = const.WD_STATE_SHADING_SETTINGS
    wd_state_time_intervals = util.get_wd_state_time_intervals(time_domain, data, end_time)
    ndsaxes.shaded_wd_state_regions = _add_wd_state_shading(ndsaxes, wd_state_time_intervals,
                                                            **const.WD_STATE_SHADING_SETTINGS)



def _has_nds_parent_figure(a):
    return isinstance(a.figure, NDSFigure)



def _update_line(pyndsfigure, channel_name, time_domain, data):
    if not channel_name in pyndsfigure.watchdog_names:
        line = pyndsfigure.line_map[channel_name]
        line.set_xdata(time_domain)
        line.set_ydata(data)


def _update_abs_threshold_line(pyndsaxes, channel_name, time_domain, data):
    # positive threshold line
    line = pyndsaxes.line_map[channel_name]
    line.set_xdata(time_domain)
    line.set_ydata(data)
    line.set_color(const.THRESHOLD_LINE_COLOR)

    # negative threshold line
    line = pyndsaxes.line_map[channel_name+'_neg']
    line.set_xdata(time_domain)
    line.set_ydata(-data)
    line.set_color(const.THRESHOLD_LINE_COLOR)



def _add_more_data(pyndsfigure):
    """
    Add more data to a figure.

    Parameters
    ----------
    pyndsfigure : :class:NDSFigure
       The axes to be updated.
    """
    try:
        xlims = np.array(pyndsfigure.get_xlim())
        xmin, xmax = np.min(xlims[:,0]), np.max(xlims[:,1])
        xmin += pyndsfigure.center_time
        xmax += pyndsfigure.center_time
        if pyndsfigure.pynds_bufferdict.add_data(xmin, xmax):
            for pyndsbuffer in pyndsfigure.pynds_bufferdict.buffers:
                time_domain = pyndsbuffer.time_domain - pyndsfigure.center_time
                end_time = pyndsbuffer.end_time-pyndsfigure.center_time
                # if pyndsbuffer.is_wd_state:
                #     _update_wd_state_shading(pyndsaxes, pyndsbuffer.channel_name, time_domain, pyndsbuffer.data,
                #                             end_time)
                if pyndsbuffer.is_abs_threshold:
                    _update_abs_threshold_line(pyndsaxes, pyndsbuffer.channel_name, time_domain, pyndsbuffer.data)
                else:
                    _update_line(pyndsfigure, pyndsbuffer.channel_name, time_domain, pyndsbuffer.data)
            return True
    except (NDSBufferDict.ChannelRequestError, NDSBufferDict.ServerDownError, NDSBufferDict.BadBufferError) as e:
        pyndsaxes.logger.warning('Received NDSBufferDict error:\n'+str(e))
    return False


class NDSAxes(Axes):
    class Exception(Exception):
        pass

    class ChannelError(Exception):
        def __init__(self, channel_names):
            self.channel_names = channel_names

    logger = util.make_default_logger('NDSAxes')

    def __init__(self, fig, rect, xmin=None, xmax=None, center_time=None, channel_names=None, initial_plot_kwargs=None,
                 figure_updated_flag_sem_pair=None, no_threshold=False,
                 no_wd_state=False, **kwargs):

        self.figure_updated_flag_sem_pair = figure_updated_flag_sem_pair

        
        if initial_plot_kwargs is None:
            initial_plot_kwargs = {}
        self.center_time = center_time
        
        # find out which channels should be shading channels
        
        s = 'Initializing NDSAxes for the following channels:'
        for channel_name in channel_names:
            s += '\n'+const.LOG_SECOND_LINE_INDENT*' '+channel_name
        NDSAxes.logger.info(s)

        Axes.__init__(self, fig, rect, **kwargs)
        self.__last_xlim = self.get_xlim()

        self.line_map = {}
        self.shaded_wd_state_regions = []
        

        self.signal_channel_names = []

        self.__initial_formatting_before_adding_channels(xmin, xmax)

        self.__annotations = []
        self.__initial_formatting_after_adding_channels()

        self.figure = fig
        
        if not _has_nds_parent_figure(self):
            self.figure.canvas.mpl_connect('button_release_event', self.figure.get_more_data)
            

    def plot_watchdog(self, channel_name, time_domain, data, end_time):
        labels, handles = [], []
        artists = []
        for state in const.WD_STATES:
            handles.append(
                Rectangle((0,0), 1, 1, facecolor=const.WD_STATE_COLOR[state], alpha=1.5*const.WD_STATE_SHADING_SETTINGS['alpha'])
            )
            labels.append(str(state))
        #legend = fig.legend(handles, labels, title='Watchdog state key')
        shade_settings = const.WD_STATE_SHADING_SETTINGS
        wd_state_time_intervals = util.get_wd_state_time_intervals(time_domain, data, end_time)
        for wdsi in wd_state_time_intervals:
            color = const.WD_STATE_COLOR[int(wdsi.state)]
            artists.append(self.axvspan(wdsi.start_time, wdsi.end_time, color=color, **shade_settings))

    def plot_state(self, channel_name, time_domain, data, end_time):
        labels, handles = [], []
        artists = []
        for state in const.WD_STATES:
            handles.append(
                Rectangle((0,0), 1, 1, facecolor=const.WD_STATE_COLOR[state], alpha=1.5*const.WD_STATE_SHADING_SETTINGS['alpha'])
            )
            labels.append(str(state))
        #legend = fig.legend(handles, labels, title='Watchdog state key')
        shade_settings = const.WD_STATE_SHADING_SETTINGS
        wd_state_time_intervals = util.get_wd_state_time_intervals(time_domain, data, end_time)
        for wdsi in wd_state_time_intervals:
            color = const.WD_STATE_COLOR[int(wdsi.state)]
            artists.append(self.add_patch(Rectangle((wdsi.start_time, 140), wdsi.end_time, 144)))
            
    def __initial_formatting_before_adding_channels(self, xmin, xmax):
        self.set_xlim(xmin-self.center_time, xmax-self.center_time)
        # TODO remove red from default color_cycle
        #self.set_color_cycle(['b', 'g', 'c', 'm', 'y', 'k','w'])

    
    def __initial_formatting_after_adding_channels(self):

        # setup viewing 
        self.autoscale_view(scalex=False)

        # show grid lines on the plot
        #self.grid(True)

        # define "x" and "y" labels
        self.set_xlabel('seconds after %f (s)' % self.center_time,
                        horizontalalignment='center')

        # setup x tick formatting
        class OffsetFormatter(Formatter):
            def __init__(self, center_time):
                self.center_time = center_time

            def __call__(self, x, pos=None):
                return ('+' if x > 0 else '') + str(x).replace('-', const.MINUS_SIGN)

        self.xaxis.set_major_formatter(OffsetFormatter(self.center_time))
        self.xaxis.set_major_locator(MaxNLocator(nbins=const.NUM_X_TICKS_BEFORE_PRUNING, prune='lower'))
        self.yaxis.set_major_locator(MaxNLocator(nbins=const.NUM_Y_TICKS_BEFORE_PRUNING, prune='both'))

        if not isinstance(self, NDSSubplot):
            self.set_title('%s (GPS %f) ' % (tconvert(str(self.center_time)),
                                             self.center_time,),
            )
            self.title.set_position(const.AXES_TITLE_POSITION)

            handles, labels = self.get_legend_handles_labels()
            indices_to_remove = []

            there_is_wd_shading = False
            for pyndsbuffer in self.pynds_bufferdict.buffers:
                if pyndsbuffer.is_wd_state:
                    there_is_wd_shading = True

            if there_is_wd_shading:
                l = _get_wd_state_key(self)

        else:
            handles, labels = self.get_legend_handles_labels()
            legend = self.legend(handles, labels)
            legend.draggable()

        self.set_ylabel('magnitude (counts)', rotation='vertical')

        self.figure.canvas.mpl_connect('pick_event', self.annotate_pick)

        
    def line_picker(self, line, mouseevent):
        """
        find the points within a certain distance from the mouseclick in
        data coords and attach some extra attributes, pickx and picky
        which are the data points that were picked
        """
        if mouseevent.xdata is None:
            return False, dict()

        mouseevent_display_coords = self.transData.transform((mouseevent.xdata, mouseevent.ydata))
        mouseevent.xdisplay = mouseevent_display_coords[0]
        mouseevent.ydisplay = mouseevent_display_coords[1]

        line_display_coords = self.transData.transform(line.get_xydata())

        maxd = 5

        ydata = line.get_ydata()
        xdisplay = line_display_coords[:, 0]
        ydisplay = line_display_coords[np.logical_and((mouseevent.xdisplay-1) <= xdisplay,
                                                      xdisplay <= (mouseevent.xdisplay+1)), 1]

        d = np.abs(ydisplay-mouseevent.ydisplay)

        ind = np.nonzero(np.less_equal(d, maxd))
        if len(ind[0]):
            pickx = mouseevent.xdata
            picky = ydata[ind[0][0]]
            props = dict(pickx=pickx, picky=picky, channel_name=line.get_label())
            return True, props
        else:
            return False, dict()
        
    def remove_annotations(self):
        while self.__annotations:
            annotation = self.__annotations.pop()
            annotation.set_visible(False)
            annotation.remove()
            del annotation

    def annotate_pick(self, event):
        self.remove_annotations()

        if hasattr(event, 'channel_name'):
            annotation = self.annotate(event.channel_name, xy=(event.pickx, event.picky),
                                       xytext=(event.pickx, event.picky),
                                       bbox=const.ANNOTATION_BBOX_ARGS,
                                       zorder=5)
            self.__annotations.append(annotation)
            self.figure.canvas.draw()

    # TODO allow plotting new watchdog state shaded regions

    def remove_channel(self, channel_name):
        NDSAxes.logger.info("Client requested to remove "+channel_name+" from an instance of NDSAxes.")
        try:
            self.pynds_bufferdict.pop(channel_name)
        except KeyError:
            NDSAxes.logger.info(channel_name+" is not plotted on this instance of NDSAxes.")
            return
        if channel_name in self.line_map:
            line = self.line_map.pop(channel_name)
            line.remove()
            return
        _remove_wd_state_shading(self)



from matplotlib import axes
NDSSubplot = axes.subplot_class_factory(NDSAxes)


class SidebarTransform(mtransforms.Transform):
    def __init__(self, fig, offset=None):
        mtransforms.Transform.__init__(self)  
        self.input_dims = 2
        self.output_dims = 2
        self.is_separable = False
        self.has_inverse = False
        self.fig = fig
        self.offset = offset

    def transform(self, values):
        ret_values = []
        for value in values:
            ret_values.append(self._transform(value))
        return np.array(ret_values)

    def _transform(self, value):
        upper_right_subplot = self.fig._get_upper_right_subplot()
        left_x_boundary = upper_right_subplot.transAxes.transform([[1,1]])[0][0]
        right_x_boundary = self.fig.transFigure.transform([[1,1]])[0][0]
        x = left_x_boundary+value[0]*(right_x_boundary-left_x_boundary)
        y = self.fig.transFigure.transform([[0,value[1]]])[0][1]
        if self.offset is not None:
            x += self.offset
        return [x,y]


class NDSFigure(Figure):
    """
    This class contains an entire figure for data from the NDS system.
    """
    logger = util.make_default_logger('NDSFigure')

    # TODO I'm not sure this is /really/ necessary...
    class Exception(Exception):
        pass

    def __repr__(self):
        return '%s(channel_names=%r, xmin=%r, xmax=%r, center_time=%r, shareax=%r, ' % (self.__class__.__name__, self.channel_names, self.xmin, self.xmax, self.center_time, self.shareax)\
                +'sharex=%r, sharey=%r, no_threshold=%r, no_wd_state=%r, ' % (self.sharex, self.sharey, self.no_threshold, self.no_wd_state)\
                +', '.join(['%s=%r' % item for item in self.__orig_kwargs.items()])+')'

    def __str__(self):
        newline = '\n          '
        r = '%s(channel_names= ... , xmin=%r, xmax=%r, center_time=%r, shareax=%r, ' % (self.__class__.__name__, self.xmin, self.xmax, self.center_time, self.shareax)\
                +'sharex=%r, sharey=%r, no_threshold=%r, no_wd_state=%r, ' % (self.sharex, self.sharey, self.no_threshold, self.no_wd_state)\
                +', '.join(['%s=%r' % item for item in self.__orig_kwargs.items()])+')'
        r = r.split(', ')
        s = self.__class__.__name__+'('
        r[0] = r[0][len(self.__class__.__name__)+1:]
        bracket_paren_stack = [1]  # mark the open parenthesis of NDSFigure()
        for part in r:
            for c in part:
                s += c
                if c in ['[','(','{']:
                    bracket_paren_stack.append(0)
                elif c in [']',')','}']:
                    if bracket_paren_stack.pop() == 1:  # if closed last parenthesis, return
                        return s
            s += ','
            if bracket_paren_stack.pop() == 1:  # check for straggling open parenthesis, curly brace, or bracket
                s += newline
                bracket_paren_stack.append(1)
            else:
                s += ' '
                bracket_paren_stack.append(0)
        return s  # should never be called if __repr__ and this function are defined correctly

    
    def __init__(self, channel_names=None,
                 watchdogs = None,
                 center_time=None, xmin=None, xmax=None,
                 title=None,
                 shareax=False, sharex=False, sharey=False,
                 subplot_shape = None,
                 no_threshold=False, no_wd_state=False,
                 subtitles = None,
                 filt = None,
                 **kwargs):
        """This object represents a figure of data from the NDS system, and
        it extends the Matplotlib Figure class to do this.

        Parameters
        ----------
        channel_names : list
           A list of the channels which should be represented in this figure.
        center_time : float
           The GPS time at which the plot is centered? I'm not 100% on this one.
           TODO Check what this actually does.
        xmin : float
           The minimum value of the x-axis.
        xmax : float
           The maximum value of the x-axis.
        title : str
           The title of the NDS plot.
        sharex : bool
           Share the x-axes?
        share-y : bool
           Share the y-axes?
        no_threshold : bool
           Apply no thresholding to the plot?
        no_wd_state : bool
           I'm not sure what this does.
           TODO Find out what this does.

        """

        Figure.__init__(self, **kwargs)

        s = 'Initializing NDSFigure with following channels:'
        for channel_name in set(cbook.flatten(channel_names)):
            s += '\n'+const.LOG_SECOND_LINE_INDENT*' '+channel_name
        NDSFigure.logger.info(s)
        self.filter = filt
        self.subplot_dimensions = subplot_shape[1:2]
        if isinstance(subtitles, list):
            self.subtitles = subtitles
        else:
            self.subtitles = [subtitles] * len(channel_names)
        self.channel_names = channel_names
        self.watchdogs = watchdogs
        self.xmin = xmin
        self.xmax = xmax
        self.xlim = [[xmin, xmax] * len(self.channel_names)]
        self.center_time = center_time
        if title:
            self.title_prefix = '%s\n' % title
        else:
            self.title_prefix = ''
        self.shareax = shareax
        self.sharex = sharex or shareax
        self.sharey = sharey or shareax
        self.subplot_updated = False
        self.no_threshold = no_threshold
        self.no_wd_state = no_wd_state
        self.__orig_kwargs = kwargs

        title = self.suptitle('{}{} (GPS {})'.format(self.title_prefix, tconvert(str(self.center_time)),
                                                     self.center_time,))
        #title.set_position(const.FIGURE_TITLE_POSITION)

        # define "x" and "y" labels
        self.text(0.5, 0.03, 'seconds after %f (s)' % self.center_time,
                  horizontalalignment='center')
        self.text(0.02, 0.5, 'magnitude (counts)', verticalalignment='center',
                  horizontalalignment='left', rotation='vertical')
        
        self.line_map = {}
        self.__wd_state_key = None
        self.__first_draw = True
        self.channel_structure = channel_names
        self.watchdog_names = watchdog_names = []
        channel_names = self.signal_channel_names = self.channel_names = [item for sublist in channel_names for item in sublist]
        if watchdogs:
            watchdog_names += [item for sublist in watchdogs for item in sublist]

            
        try:
            self.pynds_bufferdict = NDSBufferDict(channel_names+watchdog_names, xmin, xmax, filt = self.filter)
            
            #self.draw()
            #self.draw_subplots()
        except NDSBufferDict.NoNDSServerGiven as e:
            s = 'Unable to find NDS server or none specified: {}'.format(e)
            NDSAxes.logger.error(s)
            raise NDSAxes.Exception(e)

        except NDSBufferDict.ChannelRequestError as e:
            s = 'Unable to create NDSBufferDict for the following channels:'
            for channel_name in channel_names:
                s += '\n'+const.LOG_SECOND_LINE_INDENT*' '+channel_name
            NDSAxes.logger.error(s)
            raise NDSAxes.ChannelError(channel_names)

        except NDSBufferDict.ServerDownError as e:
            s = 'Unable to connect to NDS server to retrieve channel data for:'
            for channel_name in channel_names:
                s += '\n'+const.LOG_SECOND_LINE_INDENT*' '+channel_name
            NDSAxes.logger.error(s)
            raise NDSAxes.ChannelError(channel_names)

        except NDSBufferDict.BadBufferError as e:
            s = 'NDSBufferDict received a bad buffer for one of the following channels:'
            for channel_name in channel_names:
                s += '\n'+const.LOG_SECOND_LINE_INDENT*' '+channel_name
            NDSAxes.logger.error(s)
            raise NDSAxes.ChannelError(channel_names)

        self.__last_xlim = []

    def draw(self, renderer):
        """
        Draw the figure to the canvas.

        Parameters
        ----------
        renderer : ?
           The renderer to use to draw the figure.
        """
        Figure.draw(self, renderer)
        if self.__first_draw:
            self.__first_draw == False
            if self.__wd_state_key is not None:
                w = self.__wd_state_key.legendPatch.get_width()
                self.__wd_state_key.set_bbox_to_anchor((0.5, const.WD_STATE_LEGEND_Y_POS),
                                          transform=SidebarTransform(self, offset=w/2.+8))
                Figure.draw(self,renderer)
    
    def draw_subplots(self, subplot_dimensions=None):
        """
        Draw a set of subplots.
        
        Parameters
        ----------
        subplot_dimensions : tuple
           The number of rows versus the number of columns
        
        """
        if subplot_dimensions:
            subplot_dimensions = self.subplot_dimensions
        nsubplots = len(self.channel_structure)
        if subplot_dimensions is None:
            subplot_dimensions = util.get_best_subplot_dimensions(nsubplots)
        assert(isinstance(subplot_dimensions, tuple))
        assert(len(subplot_dimensions)==2)
        subplot_rows, subplot_cols = subplot_dimensions
        self.subplots = {}
        there_is_wd_shading = False
        subplot_zorder = const.SUPLOT_ZORDER_START+len(self.channel_names)
        for fake_subplot_number, subplot_channel_names in enumerate(self.channel_structure):
            subplot_number = fake_subplot_number 
            kwargs = dict(xmin=self.xmin,
                          xmax=self.xmax,
                          center_time=self.center_time,
                          channel_names=subplot_channel_names)
            if subplot_number > 0:
                if self.sharex:
                    kwargs['sharex'] = ax_to_share
                if self.sharey:
                    kwargs['sharey'] = ax_to_share

            try:
                subplot = NDSSubplot(self, subplot_rows, subplot_cols, subplot_number+1,
                                     title=self.subtitles[fake_subplot_number],
                                     no_threshold=self.no_threshold,
                                     no_wd_state=self.no_wd_state,
                                     **kwargs)
                for name in subplot_channel_names:
                    self.subplots[name] = subplot
                if isinstance(self.watchdogs, list) and len(self.watchdogs) == len(self.channel_structure):
                    for watchdog in self.watchdogs[fake_subplot_number]:
                        self.subplots[watchdog] = subplot
                    
            except NDSSubplot.ChannelError as e:
                NDSFigure.logger.critical("NDSSubplot with the channels %r has failed." % e.channel_names)
                raise NDSFigure.Exception("Could not retrieve channels: {}".format(e.channel_names))
                pass

            subplot.zorder = subplot_zorder
            subplot_zorder -= 1

            if (self.sharex or self.sharey) and subplot_number == 0:
                ax_to_share = subplot

            s = 'Adding subplot with the following channels:'
            for channel_name in subplot_channel_names:
                s += '\n'+const.LOG_SECOND_LINE_INDENT*' '+channel_name
            NDSFigure.logger.info(s)
            self.add_subplot(subplot)

            subplot.set_xlabel('')
            subplot.set_ylabel('')

        if subplot_rows in const.FIG_SUBPLOTS_ADJUST:
            self.subplots_adjust(**const.FIG_SUBPLOTS_ADJUST[subplot_rows])

        self.canvas.mpl_connect('button_release_event', self.get_more_data)
        #self.canvas.mpl_connect('button_release_event', self.remove_annotations)

        for watchdog in self.watchdogs:
            for dog in watchdog:
                pyndsbuffer = self.pynds_bufferdict[dog]
                time_domain = pyndsbuffer.time_domain-self.center_time
                end_time = pyndsbuffer.end_time-self.center_time
                data = pyndsbuffer.data
                self.subplots[pyndsbuffer.channel_name].plot_state(dog, time_domain, data, end_time)
            
        for channel_name in self.channel_names:
            pyndsbuffer = self.pynds_bufferdict[channel_name]
            time_domain = pyndsbuffer.time_domain-self.center_time
            end_time = pyndsbuffer.end_time-self.center_time
            data = pyndsbuffer.data
            self.line_map[pyndsbuffer.channel_name] = self.subplots[pyndsbuffer.channel_name].plot(time_domain, data, label=pyndsbuffer.channel_name,
                                                                                                   picker=self.subplots[pyndsbuffer.channel_name].line_picker,)[0]
            if isinstance(self, NDSSubplot):
                signal_count += 1
                
        for subplot in self.subplots.values():
            handles, labels = subplot.get_legend_handles_labels()
            subplot.legend(handles, labels)
            
    def _get_upper_right_subplot(self):
        nrows, ncols, __ = self.axes[0].get_geometry()
        last_col = ncols
        for a in self.axes:
            if a.get_geometry()[const.SUBPLOT_NUMBER_INDEX] == last_col:
                return a

    def remove_annotations(self, mouseevent):
        if mouseevent.inaxes is None:
            for a in self.axes:
                if 'remove_annotations' in dir(a):
                    a.remove_annotations()
            self.canvas.draw()

    #noinspection PyUnusedLocal
    def get_more_data(self, event):
        redraw = False
        for a in self.axes:
            if hasattr(a, 'get_more_data'):
                if a.get_more_data(None):
                    redraw = True
        if redraw:
            self.canvas.draw()

    def add_channels(self, channel_names, **kwargs):
        self.pynds_bufferdict.add_channels(channel_names, **kwargs)
        signal_count = 0
        for channel_name in channel_names:
            pyndsbuffer = self.pynds_bufferdict[channel_name]
            time_domain = pyndsbuffer.time_domain-self.center_time
            end_time = pyndsbuffer.end_time-self.center_time
            if pyndsbuffer.is_wd_state:
                _update_wd_state_shading(self, pyndsbuffer.channel_name, time_domain, pyndsbuffer.data, end_time)
            elif pyndsbuffer.is_abs_threshold:
                data = pyndsbuffer.data
                self.line_map[pyndsbuffer.channel_name+'_neg'] = self.subplots[pyndsbuffer.channel_name].plot(time_domain, -data,
                                                                           label=pyndsbuffer.channel_name,
                                                                           picker=self.line_picker, **kwargs)[0]
                data = pyndsbuffer.data
                self.line_map[pyndsbuffer.channel_name] = self.subplots[pyndsbuffer.channel_name].plot(time_domain, data, label=pyndsbuffer.channel_name,
                                                                    picker=self.line_picker, **kwargs)[0]
                _update_abs_threshold_line(self, pyndsbuffer.channel_name, time_domain, pyndsbuffer.data)
            else:
                data = pyndsbuffer.data
                self.line_map[pyndsbuffer.channel_name] = self.subplots[pyndsbuffer.channel_name].plot(time_domain, data, label=pyndsbuffer.channel_name,
                                                                    picker=self.line_picker, **kwargs)[0]
                self.signal_channel_names.append(pyndsbuffer.channel_name)
                if isinstance(self, NDSSubplot):
                    signal_count += 1

        if signal_count > 1:
            l = self.legend()
            l.draggable()

    def remove_annotations(self):
        while self.__annotations:
            annotation = self.__annotations.pop()
            annotation.set_visible(False)
            annotation.remove()
            del annotation

    def annotate_pick(self, event):
        self.remove_annotations()

        if hasattr(event, 'channel_name'):
            annotation = self.annotate(event.channel_name, xy=(event.pickx, event.picky),
                                       xytext=(event.pickx, event.picky),
                                       bbox=const.ANNOTATION_BBOX_ARGS,
                                       zorder=5)
            self.__annotations.append(annotation)
            self.figure.canvas.draw()

    # TODO allow plotting new watchdog state shaded regions

    def remove_channel(self, channel_name):
        NDSAxes.logger.info("Client requested to remove "+channel_name+" from an instance of NDSAxes.")
        try:
            self.pynds_bufferdict.pop(channel_name)
        except KeyError:
            NDSAxes.logger.info(channel_name+" is not plotted on this instance of NDSAxes.")
            return
        if channel_name in self.line_map:
            line = self.line_map.pop(channel_name)
            line.remove()
            return
        _remove_wd_state_shading(self)
    # Wake up get more data function when the x limits have changed and
    # the user releases the mouse button.
    #noinspection PyUnusedLocal
    def get_more_data(self, event):
        xlims = self.get_xlim()
        for xlim in xlims:
            if self.__last_xlim != xlim:
                NDSAxes.logger.debug("x limits have changed. Adding more data...")
                self.__last_xlim = xlim
                more_data_added = _add_more_data(self)
                if _has_nds_parent_figure(self):
                    return more_data_added
                self.canvas.draw()
            return
        NDSAxes.logger.debug("x limits have not changed.")
        return False

    def get_xlim(self):
        xlim = []
        for lims in set(self.subplots.values()):
            xlim.append(list(lims.get_xlim()))
        return xlim
