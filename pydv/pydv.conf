[servers]
None: nds.ligo.caltech.edu, 31200
L1: nds.ligo-la.caltech.edu, 31200
H1: nds.ligo-wa.caltech.edu, 31200
V1: nds.ligo.caltech.edu, 31200

[list-seismic]
name = Seismometers
parent = SEI
layout = 2,2,2,1
states = Trains
0 = L1:ISI-GND_STS_ETMY_X_BLRMS_1_3,
    L1:ISI-GND_STS_ETMY_Y_BLRMS_1_3,
    L1:ISI-GND_STS_ETMY_Z_BLRMS_1_3
0-title = End-Y
1 = L1:ISI-GND_STS_ETMX_X_BLRMS_1_3,
    L1:ISI-GND_STS_ETMX_Y_BLRMS_1_3,
    L1:ISI-GND_STS_ETMX_Z_BLRMS_1_3
1-title = End-X
1-watchdog = L1:ODC-OPERATOR_OBSERVATION_READY

[list-data]
name = Data
0 = L1:GDS-CALIB_STRAIN
0-title = Calibrated strain data
layout = 1,2,1

[list-range]
name = Range
0 = L1:DMT-SNSH_EFFECTIVE_RANGE_MPC.mean,
    H1:DMT-SNSH_EFFECTIVE_RANGE_MPC.mean,
0-title = Range
0-type = m-trend
layout = 1,2,1

[list-blrms-vertical]
name = Vertical ground motion BLRMS
shortname = Ground BLRMS vertical
layout = 2,2,2
; 0.03 Hz - 0.1 Hz
1 = L1:ISI-GND_STS_HAM2_Z_BLRMS_30M_100M,
    L1:ISI-GND_STS_ITMY_Z_BLRMS_30M_100M,
    L1:ISI-GND_STS_HAM5_Z_BLRMS_30M_100M,
    L1:ISI-GND_STS_ETMX_Z_BLRMS_30M_100M,
    L1:ISI-GND_STS_ETMY_Z_BLRMS_30M_100M
1-title = 'Earthquake band vertical ground motion (0.03\,Hz--0.1\,Hz)'
1-ylim = 3,5e3
; 0.1 Hz - 0.3 Hz
2 = L1:ISI-GND_STS_HAM2_Z_BLRMS_100M_300M,
    L1:ISI-GND_STS_ITMY_Z_BLRMS_100M_300M,
    L1:ISI-GND_STS_HAM5_Z_BLRMS_100M_300M,
    L1:ISI-GND_STS_ETMX_Z_BLRMS_100M_300M,
    L1:ISI-GND_STS_ETMY_Z_BLRMS_100M_300M
2-ylim = 1e1,8e3
2-title = 'Micro-seismic band vertical ground motion (0.1\,Hz--0.3\,Hz)'
; 0.3 Hz - 1 Hz
3 = L1:ISI-GND_STS_HAM2_Z_BLRMS_300M_1,
    L1:ISI-GND_STS_ITMY_Z_BLRMS_300M_1,
    L1:ISI-GND_STS_HAM5_Z_BLRMS_300M_1,
    L1:ISI-GND_STS_ETMX_Z_BLRMS_300M_1,
    L1:ISI-GND_STS_ETMY_Z_BLRMS_300M_1
3-ylim = 50,5e3
3-title = 'Vertical ground motion (0.3\,Hz--1\,Hz)'
; 1 Hz - 3 Hz
4 = L1:ISI-GND_STS_HAM2_Z_BLRMS_1_3,
    L1:ISI-GND_STS_ITMY_Z_BLRMS_1_3,
    L1:ISI-GND_STS_HAM5_Z_BLRMS_1_3,
    L1:ISI-GND_STS_ETMX_Z_BLRMS_1_3,
    L1:ISI-GND_STS_ETMY_Z_BLRMS_1_3
4-ylim = 10,1e4
4-title = 'Anthropogenic band vertical ground motion (1\,Hz--3\,Hz)'
; 3 Hz - 10 Hz
5 = L1:ISI-GND_STS_HAM2_Z_BLRMS_3_10,
    L1:ISI-GND_STS_ITMY_Z_BLRMS_3_10,
    L1:ISI-GND_STS_HAM5_Z_BLRMS_3_10,
    L1:ISI-GND_STS_ETMX_Z_BLRMS_3_10,
    L1:ISI-GND_STS_ETMY_Z_BLRMS_3_10
5-ylim = 20,1e4
5-title = 'Vertical ground motion (3\,Hz--10\,Hz)'
; 10 Hz - 30 Hz
6 = L1:ISI-GND_STS_HAM2_Z_BLRMS_10_30,
    L1:ISI-GND_STS_ITMY_Z_BLRMS_10_30,
    L1:ISI-GND_STS_HAM5_Z_BLRMS_10_30,
    L1:ISI-GND_STS_ETMX_Z_BLRMS_10_30,
    L1:ISI-GND_STS_ETMY_Z_BLRMS_10_30
6-ylim = 50,2e3
6-title = 'Vertical ground motion (10\,Hz--30\,Hz)'

[list-calibration]
name: Calibration
shortname: Calibration
layout=3,2
0= L1:CAL-CS_TDEP_KAPPA_TST_REAL_OUTPUT,
   L1:CAL-CS_TDEP_KAPPA_TST_IMAG_OUTPUT
0-title=CAL-CS_TDEP_KAPPA_TST
1= L1:CAL-CS_TDEP_KAPPA_PU_REAL_OUTPUT,
   L1:CAL-CS_TDEP_KAPPA_PU_IMAG_OUTPUT
1-title=CAL-CS_TDEP_KAPPA_PU
2= L1:CAL-CS_TDEP_A_REAL_OUTPUT,
   L1:CAL-CS_TDEP_A_IMAG_OUTPUT
2-title=CAL-CS_TDEP_A
3= L1:CAL-CS_TDEP_KAPPA_C_OUTPUT
3-title=CAL-CS_TDEP_KAPPA_C
4= L1:CAL-CS_TDEP_F_C_OUTPUT
4-title=CAL-CS_TDEP_F_C

[list-hwinj]
name: Hardware Injection
shortname: HWInj
layout: 1,2,3,1
1: L1:CAL-PINJX_TRANSIENT_OUTPUT
1-title: 
2: L1:CAL-PINJX_CW_OUTPUT
2-title:
3: L1:CAL-PINJX_HARDWARE_OUTPUT
3-title: 
4: L1:CAL-INJ_TRANSIENT_OUTPUT
4-title: 
5: L1:CAL-INJ_CW_OUTPUT
5-title: 
6: L1:CAL-INJ_HARDWARE_OUTPUT
6-title:

[list-bnsrange]
name: Inspiral range
shortname: range
layout: 1
1: L1:DMT-SNSH_EFFECTIVE_RANGE_MPC.mean
1-title: BNS Inspiral Range (sensemon)

[list-irigb]
name: IRIG_B timeseries
layout: 1
1: L1:CAL-PCALX_IRIGB_OUT_DQ,
   L1:CAL-PCALY_IRIGB_OUT_DQ
1-title: 

[list-pcal]
name: PCal
layout: 5,3
1: L1:CAL-PCALY_TX_PD_OUTPUT
1-title: 
2: L1:CAL-PCALY_RX_PD_OUTPUT
2-title: 
3: L1:CAL-PCALY_OFS_PD_OUTPUT
3-title: 
4: L1:CAL-PCALY_OFS_ERR_OUTPUT
4-title: 
5: L1:CAL-PCALY_OFS_AOM_DRIVE_MON_OUTPUT
5-title: 

6: L1:CAL-PCALY_EXC_SUM_MON
6-title: 
7: L1:CAL-PCALY_SWEPT_SINE_OUTPUT
7-title: 
7: L1:CAL-PCALX_TX_PD_OUTPUT
7-title: 
8: L1:CAL-PCALX_RX_PD_OUTPUT
8-title: 
9: L1:CAL-PCALX_OFS_PD_OUTPUT
9-title: 

10: L1:CAL-PCALX_OFS_ERR_OUTPUT
10-title: 
11: L1:CAL-PCALX_OFS_AOM_DRIVE_MON_OUTPUT
11-title: 
12: L1:CAL-PCALX_EXC_SUM_MON
13-title: 
14: L1:CAL-PCALX_SWEPT_SINE_OUTPUT
14-title: 

[list-sts]
name: STS
shortname: STS
layout: 1,2
1: L1:ISI-GND_STS_ETMX_X_DQ
1-title:
2: L1:ISI-GND_STS_ETMX_Y_DQ
2-title:
3: L1:ISI-GND_STS_ETMX_Z_DQ
3-title:
4: L1:ISI-GND_STS_ETMY_X_DQ
4-title:
5: L1:ISI-GND_STS_ETMY_Y_DQ
5-title:
6: L1:ISI-GND_STS_ETMY_Z_DQ
6-title:
7: L1:ISI-GND_STS_ITMY_X_DQ
7-title:
8: L1:ISI-GND_STS_ITMY_Y_DQ
8-title:
9: L1:ISI-GND_STS_ITMY_Z_DQ
9-title:
10: L1:ISI-GND_STS_HAM2_X_DQ
10-title:
11: L1:ISI-GND_STS_HAM2_Y_DQ
11-title:
12: L1:ISI-GND_STS_HAM2_Z_DQ
12-title: