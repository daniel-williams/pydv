import os
import numpy as np
from collections import namedtuple
from matplotlib.mlab import frange
import itertools

import nds2

from . import util
from . import const
import pydv

class NDSBuffer(object):
    """
    A buffer to hold data from NDS server queries.
    """

    
    class Exception(Exception):
        pass

    class SampleRateMismatch(Exception):
        pass

    class OverlapMismatch(Exception):
        pass

    logger = util.make_default_logger('NDSBuffer')
    is_wd_state = False
    is_abs_threshold = False

    def __init__(self, nds2buffer, live=False, filt=False,  is_wd_state=False, is_abs_threshold=False, *args, **kwargs):
        """
        A buffer to hold data from NDS server queries.

        Parameters
        ----------
        nds2buffer : nds data stream
           The data from a single NDS channel.
        live : bool
           Run the buffer in "live" mode, so new data is downloaded as it becomes available
        filter : list
           A zpk-format filter to be applied to the data.
        """
        self.__is_wd_state = is_wd_state
        self.__is_abs_threshold = is_abs_threshold

        
        self.channel_name = nds2buffer.channel.name
        self.sample_rate = nds2buffer.channel.sample_rate
        self.length = nds2buffer.length
        if filt:
            b, a = scipy.signal.zpk2sos(*filt)
            self.filter = (b,a)
        else:
            self.filter = None
        start_time = nds2buffer.gps_seconds + 10 ** (-9) * float(nds2buffer.gps_nanoseconds)
        time_domain = frange(start_time, start_time + (len(nds2buffer.data)) * 1. / self.sample_rate,
                             1. / self.sample_rate,
                             closed=0)
        self.__internal_buffer = np.vstack([time_domain, np.array(nds2buffer.data)])

    def __str__(self):
        s = '< name: {0:{width}s} {{fs}} '.format(self.channel_name, width=const.MAX_CHANNEL_NAME_WIDTH) \
            + 'sample rate = {0:{width}.1f} {{fs}} '.format(self.sample_rate, width=const.MAX_SAMPLE_RATE_DIGITS) \
            + 'start time = {0:f} {{fs}} end time = {1:f} >'.format(self.start_time, self.end_time)
        s = s.format(fs='#')
        return s

    def get_data(self, start_time, end_time):
        """
        Return data from the internal buffer between two different GPS times.

        Parameters
        ----------
        start_time : float
           The starting GPS time. Note that this will be rounded to the nearest second.
        end_time : float
           The ending GPS time. Note that this will be rounded to the nearest second.

        Returns
        -------
        data : ndarray
           The ndarray containing the data between the two times.
        """
        # TODO Remove asserts; these should be replaced with proper error handling
        assert (start_time < end_time)
        assert (start_time >= self.start_time)
        assert (end_time <= self.end_time)

        start_index = self.__time_to_index(start_time)
        end_index = self.__time_to_index(end_time)
        assert (self.__internal_buffer.shape[1] >= end_index - start_index)
        return self.__internal_buffer[1, start_index:end_index]
    
    def get_time_domain(self, start_time, end_time):
        """
        Return the timestamps from the internal buffer between two GPS times.

        Parameters
        ----------
        start_time : float
           The starting GPS time. Note that this will be rounded to the nearest second.
        end_time : float
           The ending GPS time. Note that this will be rounded to the nearest second.

        Returns
        -------
        times : ndarray
           The ndarray containing the timestamps between the two times.
        """
        start_index = self.__time_to_index(start_time)
        end_index = self.__time_to_index(end_time)
        assert (self.__internal_buffer.shape[1] >= end_index - start_index)
        return self.__internal_buffer[0, start_index:end_index]
    
    def __time_to_index(self, time):
        """
        Calculates the index which relates to a given GPS time.

        Parameters
        ----------
        time : float
           A GPS time.
        
        Returns
        -------
        index : int
           The index of the internal buffer which corresponds to the requested
           gps time.
        """
        return int(np.floor(self.sample_rate * (time - self.start_time)))

    @property
    def is_wd_state(self):
        return self.__is_wd_state

    @is_wd_state.setter
    def is_wd_state(self, b):
        assert (type(b) == bool)
        self.__is_wd_state = b

    @property
    def is_abs_threshold(self):
        return self.__is_abs_threshold

    @is_abs_threshold.setter
    def is_abs_threshold(self, b):
        assert (type(b) == bool)
        self.__is_abs_threshold = b


    @property
    def data(self):
        if not self.filter:
            return self.__internal_buffer[1, :]
        else:
            return scipy.signal.filtfilt(self.filter[0], self.filter[1], self.__internal_buffer[1,:])

    @property
    def time_domain(self):
        return self.__internal_buffer[0, :]

    @property
    def start_time(self):
        return self.__internal_buffer[0, 0]

    @property
    def end_time(self):
        return self.__internal_buffer[0, -1] + 1. / self.sample_rate

    def __add__(self, other):
        """
        Allow two buffers to be added using the standard Pythonic syntax.
        """
        NDSBuffer.logger.debug('Combining two buffers with channel names:'
                               + '\n' + const.LOG_SECOND_LINE_INDENT * ' ' + self.channel_name
                               + '\n' + const.LOG_SECOND_LINE_INDENT * ' ' + other.channel_name)

        # Perform some sanity checks on the data

        # Check if the two buffers contain the same channels, We can
        # do this either by checking that the channel names are
        # identical, or we can check that they contain the same
        # information.
        if self.channel_name != other.channel_name:
            NDSBuffer.logger.debug('The channels names do not match, but if the data is the same,'
                                + 'then everything should be okay.')

        # Check the sample rates are the same.
        if other.sample_rate != self.sample_rate:
            NDSBuffer.logger.debug('Cannot combine buffers with different sample rates.')
            NDSBuffer.logger.debug('Raising SampleRateMismatch.')
            raise NDSBuffer.SampleRateMismatch
        sample_rate = self.sample_rate

        # Check that the overlaps match
        if other.end_time < self.start_time or other.start_time > self.end_time:
            NDSBuffer.logger.debug('Cannot combine buffers whose overlapping entries do not match.')
            NDSBuffer.logger.debug('Raising OverlapMismatch.')
            raise NDSBuffer.OverlapMismatch

        # other.__internal_buffer[1,other_offset] == self.__internal_buffer[1,0]
        merged_start_time = min([self.start_time, other.start_time])
        merged_end_time = max([self.end_time, other.end_time])

        list_to_return = self
        list_to_merge = other
        merged_buffer = self.__internal_buffer

        class MergedBuffers(Exception):
            # TODO Make it clear what this exception actually means...
            pass

        try:
            # Check if one of the buffers completely encompasses the other.
            if other.start_time <= self.start_time and other.end_time >= self.end_time:
                merged_buffer = other.__internal_buffer
                raise MergedBuffers
            elif self.start_time <= other.start_time and self.end_time >= other.end_time:
                raise MergedBuffers

            # Make the new buffer
            merged_buffer = np.zeros((2, int(sample_rate * (merged_end_time - merged_start_time))))
            
            if other.start_time > self.start_time:
                list_to_return = other
                list_to_merge = self

            # Perform the merge
            merged_buffer[:, :list_to_merge.__internal_buffer.shape[1]] = list_to_merge.__internal_buffer[:, :]
            offset = int(sample_rate * (list_to_merge.end_time - list_to_return.start_time))
            merged_buffer[:, list_to_merge.__internal_buffer.shape[1]:] = list_to_return.__internal_buffer[:, offset:]
            # Eughhhhh we're raising exceptions to verify the correct running of the code?!!
            # TODO FIX ME
            raise MergedBuffers
        except MergedBuffers:
            pass

        list_to_return.__internal_buffer = merged_buffer
        if list_to_return.is_wd_state or list_to_merge.is_wd_state:
            list_to_return.is_wd_state = True
        if list_to_return.is_abs_threshold or list_to_merge.is_abs_threshold:
            list_to_return.is_abs_threshold = True

        NDSBuffer.logger.debug('Successfully combined the buffers.')
        NDSBuffer.logger.debug('Returned buffer has channel name %s and contains data from %d to %d.'
                               % (list_to_return.channel_name, list_to_return.start_time, list_to_return.end_time))
        return list_to_return

    def __radd__(self, other):
        return self.__add__(other)


# This looks like something e might want to get rid of
#HostPortPair = namedtuple('HostPortPair', ['host', 'port'])


class NDSBufferDict(dict):
    class Exception(Exception):
        pass

    class NoNDSServerGiven(Exception):
        pass

    class ServerDownError(Exception):
        pass

    class ChannelRequestError(Exception):
        pass

    class FetchedChannelNameOrderMismatch(Exception):
        pass

    class BadBufferError(Exception):
        pass

    class StartTimeAfterEndTime(Exception):
        pass

    logger = util.make_default_logger('NDSBufferDict')

    def __init__(self, channel_names, start_time, end_time, wd_state_mask=None, abs_threshold_mask=None, filt=None):
        """
        Create a dictionary of buffers to allow multiple channels' worth of data to be stored side-by-side.

        Parameters
        ----------
        channel_names : list
           The channel names which should be used.
        start_time : float
           The desired GPS start time of the data.
        end_time : float
           The desired GPS end time of the data.

        
        """
        if filt:
            self.filter = filt
        else:
            self.filter = None
        super(NDSBufferDict, self).__init__()
        if start_time >= end_time:
            raise NDSBufferDict.StartTimeAfterEndTime
        NDSBufferDict.logger.debug('Established connection.')
        self.__start_time = start_time
        self.__end_time = end_time
        
        self.add_channels(channel_names, wd_state_mask=wd_state_mask, abs_threshold_mask=abs_threshold_mask)

    @property
    def start_time(self):
        return self.__start_time

    @start_time.setter
    def start_time(self, time):
        if time >= self.__end_time:
            raise NDSBufferDict.StartTimeAfterEndTime
        if time < self.__start_time:
            self.add_data(time, self.__start_time)

    @property
    def end_time(self):
        return self.__end_time

    @end_time.setter
    def end_time(self, time):
        if time <= self.__start_time:
            raise NDSBufferDict.StartTimeAfterEndTime
        if time > self.__end_time:
            self.add_data(self.__end_time, time)

    @property
    def channel_names(self):
        return self.keys()


    def connection(self, site="None"):
        """
        Make a connection to the appropriate site's server in order to
        collect data from it.

        Parameters
        ----------
        site : str
           The two-character abbreviation for the site, e.g. L1 for the 
           LIGO Livingston 4-km detector.

        Returns
        -------
        connection : `nds2.connection` object
           The connection.
        """
        server, port = pydv.get_server(site)
        connection = nds2.connection(server, port)
        connection.set_parameter('GAP_HANDLER','STATIC_HANDLER_NAN')
        return connection


    def _fetch_data(self, t_start, t_end, channel_names, wd_state_mask=None, abs_threshold_mask=None):
        """
        Pull data from the NDS client into the underlying buffers for a given set of channels.

        Parameters
        ----------
        t_start : float
           The start GPS time of the desired data.
        t_end : float
           The end GPS time of the desired data.
        channel_names : list or str
           The names of the channels to be added to the buffer.

        Todo
        ----
        May need to re-implement some of the masking functionality which I took out.
        """
        
        if type(channel_names)==str:
            channels = [channels]
        
        def check_mask(mask):
            if mask is None:
                mask = [False] * len(channel_names)
            else:
                assert (len(mask) == len(channel_names))
            return mask

        wd_state_mask = check_mask(wd_state_mask)
        abs_threshold_mask = check_mask(abs_threshold_mask)

        connection = self.connection()
        

        chan_by_ifo = {}
        fetched_channel_data = {}


        # fetch requires integer arguments, so we use the
        # floor and ceiling of start_time and end_time respectively
        requested_t_start = int(np.floor(float(t_start)))
        requested_t_end = int(np.ceil(float(t_end)))

        # If a minute trend is requested it must be for a multiple of 60 seconds
        for i, channel in enumerate(channel_names):
            if len(channel.split("."))>1:
                print "Found something which is a trend {}".format(channel)
                if len(channel.split("."))==2:
                       print "Assume a minute trend"
                       # Make sure that we ask for a whole number of minutes' worth of data
                       difference = (requested_t_end - requested_t_start)%60
                       
                       requested_t_start -= requested_t_start%60
                       requested_t_end += 60-requested_t_end%60
        
        msg = "Buffers requested at start time %d and end time %d for the following channels:" % (requested_t_start, requested_t_end)
        for name in channel_names:
            msg += "\n" + const.LOG_SECOND_LINE_INDENT * ' ' + name
            NDSBufferDict.logger.info(msg)
        
        for channel in channel_names:
            # We may want to make queries to the correct NDS servers for each IFO, work out what those are
            ifo = channel[:2]
            if ifo not in chan_by_ifo:
                chan_by_ifo[ifo] = []
            chan_by_ifo[ifo].append(channel)

        for ifo in chan_by_ifo.keys():
            channels = chan_by_ifo[ifo]
            ifo_data = self.connection(ifo).fetch(requested_t_start, requested_t_end, channels)
            for i, channel in enumerate(channels):
                fetched_channel_data[channel] = ifo_data[i]
                msg = "Fetching buffers for the following channels from %d to %d:" \
                          % (requested_t_start, requested_t_end)
            for channel_name in channels:
                msg += '\n' + const.LOG_SECOND_LINE_INDENT * ' ' + channel_name
                NDSBufferDict.logger.debug(msg)
                
        for k, ndsbuffer in enumerate(fetched_channel_data):
            mask_idx = k #  const.NDS_REQUEST_LIMIT * j + k
            fetched_channel_data[ndsbuffer] = pyndsbuffer = NDSBuffer(fetched_channel_data[ndsbuffer], is_wd_state=wd_state_mask[mask_idx],
                                                                      is_abs_threshold=abs_threshold_mask[mask_idx], filt=self.filter)
            NDSBufferDict.logger.debug("Received %s" % pyndsbuffer)

            if requested_t_start < pyndsbuffer.start_time:
                NDSBufferDict.logger.warning("Requested buffer start time at %d" % requested_t_start
                                             + " but received a buffer with start time %d"
                                             % pyndsbuffer.start_time)
                #raise BufferDict.BadBufferError
            if requested_t_end > pyndsbuffer.end_time:
                NDSBufferDict.logger.warning("Requested buffer end time at %d" % requested_t_end
                                             + " but received a buffer with end time %d"
                                             % pyndsbuffer.end_time)


        msg = 'Summary of time series buffers received:'
        for channel_name in fetched_channel_data.keys():
            msg += '\n' + const.LOG_SECOND_LINE_INDENT * ' ' + channel_name
        NDSBufferDict.logger.debug(msg)

        # we probably don't want to close the connection automatically.
        # connection.close()

        return fetched_channel_data

    def add_data(self, t_start, t_end):
        """
        Add data from the NDS connection to the buffers.
        """
        def fetch(t_start, t_end):
            fetched_channel_data = self._fetch_data(t_start, t_end, self.channel_names)
            for channel_name in self.channel_names:
                self[channel_name] += fetched_channel_data[channel_name]
        r = False
        if t_start < self.__start_time:
            fetch(t_start, self.__start_time)
            self.__start_time = t_start
            r = True
        if t_end > self.__end_time:
            fetch(self.__end_time, t_end)
            self.__end_time = t_end
            r = True
        return r

    def add_channels(self, channel_names, wd_state_mask=None, abs_threshold_mask=None):
        """
        Add channels to the buffer dictionary.

        Parameters
        ----------
        channel_names : list or str
           The names of the channels to be added to the buffer.
        """
        assert (type(channel_names) == list)
        if wd_state_mask is not None:
            assert (len(wd_state_mask) == len(channel_names))
        if abs_threshold_mask is not None:
            assert (len(abs_threshold_mask) == len(channel_names))

        channel_names_to_fetch = []
        num_removed = 0
        for i, channel_name in enumerate(channel_names):
            if channel_name not in self.channel_names:
                channel_names_to_fetch.append(channel_name)
            else:
                if wd_state_mask is not None:
                    wd_state_mask.pop(i - num_removed)
                if abs_threshold_mask is not None:
                    abs_threshold_mask.pop(i - num_removed)
                num_removed += 1
        if not channel_names_to_fetch:
            return
        fetched_channel_data = self._fetch_data(self.__start_time, self.__end_time, channel_names_to_fetch,
                                                 wd_state_mask=wd_state_mask,
                                                 abs_threshold_mask=abs_threshold_mask)
        for channel_name in channel_names_to_fetch:
            self[channel_name] = fetched_channel_data[channel_name]

        #self.channel_names += channel_names

    def add_wd_state_channel(self, channel_name):
        self.add_channels(list(channel_name), wd_state_mask=[True])

    @property
    def buffers(self):
        return self.values()

class NDSBufferIterator(object):
    """
    Produce an iterating NDS Buffer, for use in monitors.
    """

    def __init__(self, bufferdict):
        """
        Parameters
        ----------
        bufferdict : NDSBufferDict object
           The buffer dictionary which is being updated.
        """
        self.iterators = []
        chan_by_ifo = {}
        self.bufferdict = bufferdict
        
        
    def start(self):
        """
        Iterate the contents of this buffer to allow the data to be updated.
        """
        for ifo_data in self.bufferdict.connection("None").iterate(self.bufferdict.channel_names):
            for i, channel in enumerate(channels):
                fetched_channel_data[channel] = ifo_data[i]
                self.bufferdict[channel] += NDSBuffer(ifo_data[i])
                msg = "Fetching buffers for the following channels from %d to %d:" \
                          % (requested_t_start, requested_t_end)
