
from matplotlib import (rcParams, rc_params)
# set default params
GWPY_PLOT_PARAMS = {
    'axes.grid': True,
    'axes.axisbelow': False,
    'axes.formatter.limits': (-3, 4),
    'axes.labelsize': 22,
    'axes.titlesize': 26,
    'grid.linestyle': ':',
    'grid.linewidth': .5,
    'image.aspect': 'auto',
    'image.interpolation': 'nearest',
    'image.origin': 'lower',
    'xtick.labelsize': 20,
    'ytick.labelsize': 20,
}

# construct new default color cycle

# set mpl version dependent stuff
# try:
#     from matplotlib import cycler
# except (ImportError, KeyError):  # mpl < 1.5
#     GWPY_PLOT_PARAMS['axes.color_cycle'] = GWPY_COLOR_CYCLE
# else:
#     GWPY_PLOT_PARAMS.update({
#         'axes.prop_cycle': cycler('color', GWPY_COLOR_CYCLE),
#     })

# update matplotlib rcParams with new settings
rcParams.update(GWPY_PLOT_PARAMS)

# fix matplotlib issue #3470
if rcParams['font.family'] == 'serif':
    rcParams['font.family'] = u'serif'
