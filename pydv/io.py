"""

This file is part of pydv, the pythonic dataviewer for
Gravitational wave detectors.

Author: Daniel Williams <daniel.williams@ligo.org>

IO
--

This file contains the IO objects to allow pydv to interact with the
NDS server in an efficient manner.

"""
import pydv
import nds2
import numpy as np

class NDSClient(object):
    """Provides a light-weight interface to the NDS client, and provides a
    means of loading data requests into buffers for plotting."""

    def __init__(self):
        pass

    def connection(self, site):
        """
        Make a connection to the appropriate site's server in order to
        collect data from it.

        Parameters
        ----------
        site : str
           The two-character abbreviation for the site, e.g. L1 for the 
           LIGO Livingston 4-km detector.

        Returns
        -------
        connection : `nds2.connection` object
           The connection.
        """
        server, port = pydv.get_server(site)
        connection = nds2.connection(server, port)
        return connection

    def fetch(self, channels, start, end):
        """Fetch data from a list of channels over a specified period of
        time. If the end time is omitted then the end is assumed to be
        now, and the data will refresh to add new data as it becomes
        available. Likewise if the start time is omitted the start
        time is assumed to be now, and the stream will continue to
        update every second.

        Parameters
        ----------
        channels : `str` or `list`
           The channel list. If only one channel is desired this can be 
           provided as a string rather than a list.
        start : int, optional
           The start time of the data.
        end : int, optional
           The end time of the data.

        Returns
        -------
        ndsdata
        """

        # we need to sort the channels into groupings by
        # interferometer first so that we can request the correct server

        chan_by_ifo = {}
        data = {}
        if type(channels)==str:
            channels = [channels]
        for channel in channels:
            ifo = channel[:2]
            if ifo not in chan_by_ifo:
                chan_by_ifo[ifo] = []
            chan_by_ifo[ifo].append(channel)

        for ifo in chan_by_ifo.keys():
            channels = chan_by_ifo[ifo]
            ifo_data = self.connection(ifo).fetch(start, end, channels)
            for i, channel in enumerate(channels):
                data[channel] = ifo_data[i]

        return data


class RingBuffer(object):
    """
    An implementation of a simple ring buffer; as data is added to the
    end data at the beginning of the buffer is overwritten, so that
    the entire buffer keeps a constant length.

    https://scimusing.wordpress.com/2013/10/25/ring-buffers-in-pythonnumpy/
    """
    def __init__(self, length):
        """
        A ring buffer which contains a fixed amount of data.
        As data is added to the end the data at the beginning is removed.

        Parameters
        ----------
        length : int
           The length, in samples, of the buffer.
        """
        self.data = np.zeros(length, dtype='f') 
        self.index = 0

    def __repr__(self):
        return self.get()
        
    def extend(self, x):
        """
        Add new data to the end of the buffer.

        Parameters
        ----------
        x : np.ndarray
           The array containing the new data to be added to the buffer.
        """
        "adds array x to ring buffer"
        x_index = (self.index + np.arange(x.size)) % self.data.size
        self.data[x_index] = x
        self.index = x_index[-1] + 1

    def get(self):
        "Returns the first-in-first-out data in the ring buffer."
        idx = (self.index + np.arange(self.data.size)) %self.data.size
        return self.data[idx]

    def setLength(self, new_length):
        """
        Change the length of the ring buffer.
        if the new buffer is shorter than the old one it will be 
        truncated at the left side; otherwise the new buffer will 
        be left-padded with zeros.
        
        Parameters
        ----------
        new_length : int
           The length of the new buffer.
        """
        newdata = np.zeros(new_length)
        idx = (self.index + np.arange(self.data.size)) %self.data.size
        if len(newdata) > len(self.data):
            newdata[-len(newdata):] = self.data[idx]
        else:
            cutoff = len(self.data) - len(newdata)
            newdata = self.data[idx][cutoff:]#[::-1]
        self.data = newdata
        self.index = 0
