#!/usr/bin/env python
import code

import argparse
import signal

import gpstime

from . import plot
from pydv import PACKAGE_NAME, CONFIGURATION
from pydv.parsers import channel_list_parser
import matplotlib.pyplot as plt

import os.path
import re

from multiprocessing import Process, Queue

##################################################


description = """Plot time series of NDS channels.
"""
summary = description.splitlines()[0]

def main():
    bigparser = argparse.ArgumentParser(description=description.strip(),
                                     prog = PACKAGE_NAME,
                                     formatter_class=argparse.RawDescriptionHelpFormatter)

    bigparser.add_argument('--sharex', action='store_true', default=True, help='share the x axis on all subplots')
    bigparser.add_argument('--sharey', action='store_true', help='share the y axis on all subplots')
    bigparser.add_argument('--time', type=float, default=None, help='end time for all channels (default: now)')
    bigparser.add_argument('--title', type=str, default=None, help='plot title')
    bigparser.add_argument('--lookback', type=float, default=10, help='time to look behind --time')
    bigparser.add_argument('--lookforward', type=float, default=0, help='time to look ahead of --time')
    bigparser.add_argument('--no-threshold', action='store_true', help='do not try to parse threshold channel names')
    bigparser.add_argument('--no-wd-state', action='store_true', help='do not try to parse watchdog state channel names')


    
    subparsers = bigparser.add_subparsers(help='Sub-command help')
    
    parser_list = subparsers.add_parser("list", help = "Plot a channel list")
    parser_list.add_argument("list", help = "Either a channel list file, or the name of a stored list.", metavar="channel_list")

    parser_raw = subparsers.add_parser("channels", help = "Plot a list of channels entered onto the command line.")
    parser_raw.add_argument('channel_names', metavar='channel', type=str, help='channel name')
    parser_raw.add_argument('--debug', action='store_true', help='flag for very verbose output')
    
    args = bigparser.parse_args()

    if "list" in args:
        # Looks like we've found a channel list First look to see if
        # the provided argument corresponds to a file; if it doesn't
        # then look in the configuration file to see if it's been
        # saved there by the user.
        if os.path.isfile(args.list):
            channel_list_parser(args.list)
        else:
            channel_list = dict(CONFIGURATION.items("list-{}".format(args.list)))
            r = re.compile('^([0-9]+)$')
            keys = filter(r.match, channel_list.keys())
            channels = [channel_list[key].replace("\n","").split(",") for key in keys]
            t = re.compile('^(?:[0-9]+)-title$')
            titles = []
            for key in filter(t.match, channel_list.keys()):
                titles.append(channel_list[key])
            watchdogs = []
            for key in keys:
                if "{}-watchdog".format(key) in channel_list:
                    watchdogs.append(channel_list["{}-watchdog".format(key)].split(","))
                else:
                    watchdogs.append([])
            big_title = channel_list['name']
            subplot_shape = channel_list['layout']
    else:
        titles = ""
        watchdogs = []
        # Remove the subplot command in favour of expressing the channel
        # names as a comma separated list, with plus signs separating the
        # subplots.
        channels = []
        #print args.channel_names
        channels_subs = args.channel_names.split("+")
        for sub in channels_subs:
            channels.append(sub.split(","))
        subplot_shape = 1,len(channels)
        if "title" in args:
            big_title = args.title
        else:
            big_title = "Data"
    print "watchdogs = {}".format(watchdogs)
    if args.time is None:
        args.time = int(gpstime.gpsnow())

    # for cleaner C-c handling
    signal.signal(signal.SIGINT, signal.SIG_DFL)
    
    plot(
        channel_names=channels,
        watchdogs = watchdogs,
        subplot_shape = subplot_shape,
        xmin = args.time - args.lookback,
        xmax = args.time + args.lookforward,
        center_time=args.time,
        title=big_title,
        sharex=args.sharex,
        sharey=args.sharey,
        no_threshold=args.no_threshold,
        no_wd_state=args.no_wd_state,
        subtitles = titles,
        )
    plt.subplots_adjust(left=0.1, bottom=0.05, right=0.95, top=0.95, wspace=0.02, hspace=0.02)
    
    
if __name__ == '__main__':

    p = Process(target=main)
    p.start()
    code.InteractiveConsole(locals=globals()).interact()
    p.join()
