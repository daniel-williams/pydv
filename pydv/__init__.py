"""
pydv - A Pythonic Dataviewer for Python
"""

import os
import logging
import matplotlib.pyplot as plt

from .version import __version__
from .plotting import NDSFigure, NDSAxes
from .bufferdict import NDSBufferDict, NDSBuffer
from .exceptions import ConfigurationNotFound
from .parsers import channel_list_parser 

import ConfigParser

# Data about the package
# This might potentially make better sense somewhere else.
PACKAGE_NAME = "pydv"

DEFAULT_LOG_LEVEL = 'INFO'

# Read-in the configuration for the package from its conf file
from pkg_resources import resource_string, resource_stream, resource_filename
default_config = resource_stream(__name__, '{}.conf'.format(PACKAGE_NAME))
CONFIGURATION = ConfigParser.ConfigParser()
CONFIGURATION.readfp(default_config)
CONFIGURATION.read([os.path.join(direc, ".{}".format(PACKAGE_NAME)) for direc in (os.curdir, os.path.expanduser("~"), "/etc/{}".format(PACKAGE_NAME))])


def get_server(site="None"):
    """
    Convenience function to get the server address and port from the
    configuration files for a given interferometer.

    Parameters
    ----------
    site : `str`
       The site code which is to be looked-up, for example L1 for the 4km detector at Livingston, Louisiana.

    Returns 
    -------
    server : `str`
       The server address for the site
    port : int
       The port number to communicate on
    """
    
    try:
        server, port = CONFIGURATION.get("servers", site).split(',')
    except:
        raise ConfigurationNotFound

    return server, int(port)

    
def plot(
        channel_names,
        center_time,
        xmin,
        xmax,
        title=None,
        sharex=True,
        sharey=False,
        no_threshold=True,
        no_wd_state=True,
        save=None,
        show=True,
        subplot_shape = None,
        subtitles = None,
        watchdogs = None,
        filt = None,
        ):

    """
    The main plotting script for the entire package.
    """
    def _addHandler(logger):
        formatter = logging.Formatter('%(asctime)s : %(name)s : %(message)s')
        handler = logging.StreamHandler()
        handler.setFormatter(formatter)
        handler.set_name('tests')
        logger.addHandler(handler)
        logger.setLevel(os.getenv('PYDV_LOG_LEVEL', DEFAULT_LOG_LEVEL).upper())

    _addHandler(NDSAxes.logger)
    _addHandler(NDSFigure.logger)
    _addHandler(NDSBufferDict.logger)
    _addHandler(NDSBuffer.logger)

    fig = plt.figure(FigureClass=NDSFigure,
                     channel_names=channel_names,
                     watchdogs = watchdogs,
                     center_time=center_time,
                     xmin=xmin,
                     xmax=xmax,
                     title=title,
                     subtitles = subtitles,
                     sharex=sharex,
                     sharey=sharey,
                     no_threshold=no_threshold,
                     subplot_shape = subplot_shape,
                     filt = filt, 
                     no_wd_state=no_wd_state,)
    try:
        fig.draw_subplots()
        
        #fig.add_channels(fig.channel_names)#, **initial_plot_kwargs)
    except NDSAxes.Exception as e:
        raise SystemExit("Failed retrieving data or drawing plot: {}".format(e))
    except NDSFigure.Exception as e:
        raise SystemExit("Failed retrieving data or drawing plot: {}".format(e))

    fig.set_size_inches(16*1.5, 9*1.5, forward=True)
    
    if save:
        plt.savefig(save)
    if show:
        plt.show()

    return fig
