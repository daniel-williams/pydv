import ConfigParser

def channel_list_parser(filename):
    """
    Parse a pydv-style channel list and return the appropriate 
    values to produce plots.
    """
    
    channellist = ConfigParser.ConfigParser()
    with open(filename, 'r') as filehandle:
        channellist.readfp(filehandle)
    
    channel_list = dict(channellist.items("list"))
    r = re.compile('^([0-9]+)$')
    keys = filter(r.match, channel_list.keys())
    channels = [channel_list[key].replace("\n","").split(",") for key in keys]
    t = re.compile('^(?:[0-9]+)-title$')
    titles = [channel_list[key] for key in filter(t.match, channel_list.keys())]
    f = re.compile('^(?:[0-9]+)-filters$')
    filters = [channel_list[key] for key in filter(f.match, channel_list.keys())]
    watchdogs = []
    for key in keys:
        if "{}-watchdog".format(key) in channel_list:
            watchdogs.append(channel_list["{}-watchdog".format(key)].split(","))
        else:
            watchdogs.append([])
    big_title = channel_list['name']
    subplot_shape = channel_list['layout']
    return channels, title, watchdogs, big_title, subplot_shape, filters
