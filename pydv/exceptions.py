"""
EXCEPTIONS

This file is part of pydv

"""

class ConfigurationNotFound(Exception):
    pass
