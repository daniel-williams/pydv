import matplotlib.pyplot as plt
import matplotlib
import numpy as np
import nds2
import collections
import sys
from PyQt4 import QtGui, QtCore
from matplotlib.figure import Figure
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
import threading

progname = "pydv sandbox"

conn = nds2.connection('l1nds1', 8088)
raw_rate = 16384.
refresh_rate = 30.
decimation = 1
samples = raw_rate / refresh_rate
secondsbefore = 100

channel_list = ["L1:PEM-CS_SEIS_LVEA_VERTEX_X", "L1:PEM-CS_SEIS_LVEA_VERTEX_Y", "L1:CDS-SENSEMON_CAL_L1_SNSW_EFFECTIVE_RANGE_MPC"]


class RingBuffer(object):
    """
    An implementation of a simple ring buffer; as data is added to the
    end data at the beginning of the buffer is overwritten, so that
    the entire buffer keeps a constant length.

    https://scimusing.wordpress.com/2013/10/25/ring-buffers-in-pythonnumpy/
    """
    def __init__(self, length):
        self.data = np.zeros(length, dtype='f') 
        self.index = 0

    def extend(self, x):
        "adds array x to ring buffer"
        x_index = (self.index + np.arange(x.size)) % self.data.size
        self.data[x_index] = x
        self.index = x_index[-1] + 1

    def get(self):
        "Returns the first-in-first-out data in the ring buffer"
        idx = (self.index + np.arange(self.data.size)) %self.data.size
        return self.data[idx]

        
        timer.timeout.connect(self.update_figure)
        timer.start(1000)


colors = RingBuffer(12)
colors.data = np.array(['#a6cee3','#1f78b4','#b2df8a','#33a02c','#fb9a99','#e31a1c','#fdbf6f','#ff7f00','#cab2d6','#6a3d9a','#ffff99','#b15928'])

class LivePlotter(object):
    """
    Live plots a series from the NDS connection.
    """
    def __init__(self, channel_list, look_back = 10, address = None, port = 8088):
        """
        Make a plot of the live data from the NDS connection. 

        Parameters
        ----------
        channel_list : list
           A list of channels which should be plotted.
        look_back : int
           The number of seconds into the past to be saved.
        address : str
           The address of the NDS server
        port : int
           The port number to communicate with the NDS server on.
        """
        self.conn = nds2.connection('l1nds1', 8088)
        self.channel_list = channel_list
        self.buffers = []
        self.times = []
        secondsbefore = look_back
        # Set up a bank of buffers to pull the channel information into
        for i in range(len(channel_list)):
            raw_rate = self.conn.find_channels(self.channel_list[i])[0].sample_rate
            self.buffers.append(RingBuffer(length=secondsbefore*raw_rate))
            self.times.append(np.linspace(-secondsbefore, 0, secondsbefore*raw_rate))

        n_channels = len(channel_list)
        f, self.axes = plt.subplots(n_channels)
        f.text(0.5, 0.04, "[seconds]", ha='center')
        
    def run(self):
        """
        Set the plot running. 
        """
        for ret in conn.iterate(self.channel_list):
            self.update(ret)
    def update(self, ret):
        """
        Update the plot with new information.

        Parameters
        ----------
        ret : RingBuffer object
           The ringbuffer containing the channel data.
        """
        ax = self.axes
        for i in range(len(self.buffers)):
            self.buffers[i].extend(ret[i].data)
            ax[i].cla()
            ax[i].set_ylabel(ret[i].channel.signal_units)
            ax[i].plot(self.times[i], self.buffers[i].get()[::decimation], color=colors.get()[i], label=ret[i].channel.name)
            ax[i].legend()
        #plt.pause(1.0)



plotter = LivePlotter(channel_list)
t = threading.Thread(target = plotter.run)
t.start()
